//
//  ToDoViewControllerCD.swift
//  Skillbox14.2
//
//  Created by fomich on 18.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit
import CoreData

class ToDoViewControllerCD: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var toDoTextField: UITextField!
    
    var todos = [ToDoList]()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func saveToDos(){
        do{
            try context.save()
        }catch{
            print("Error saving new todo")
        }
        tableView.reloadData()
    }
    
    func loadToDos(){
        let request: NSFetchRequest<ToDoList> = ToDoList.fetchRequest()
        
        do{
            todos = try context.fetch(request)
        }catch{
            print("Error fetching data")
        }
        tableView.reloadData()
    }
    
    @IBAction func toDoAdd(_ sender: Any) {
        
        if toDoTextField.text?.isEmpty ?? true {
            mainLabel.text = "Fill the field below"
        } else {
            let newToDo = ToDoList(context: self.context)
            newToDo.name = toDoTextField.text
            self.todos.append(newToDo)
            self.saveToDos()
            toDoTextField.text = nil
            tableView.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadToDos()

        // Do any additional setup after loading the view.
    }
}

extension ToDoViewControllerCD: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoCellCD") as! ToDoCellCD
        
        let cells = todos[indexPath.row]
        
        cell.toDoText.text = cells.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete{

            let cell = todos[indexPath.row]
            todos.remove(at: indexPath.row)
            context.delete(cell)
            do{
                try context.save()
            }catch{
                print("Error deleting todo")
            }
            tableView.reloadData()
        }
    }
}
