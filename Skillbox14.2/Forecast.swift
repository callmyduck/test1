//
//  Forecast.swift
//  Skillbox14.2
//
//  Created by fomich on 18.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

class Forecast {
    
    let temp: Int
    let date: String
    
    init?(data: NSDictionary){
        guard
            let main = data["main"] as? NSDictionary,
            let temp = main["temp"] as? NSNumber,
            let date = data["dt_txt"] as? String
            else{
                return nil
        }
        self.temp = temp.intValue - 273
        self.date = date
        PersistanceWeather.shared.addForecast(temp: temp.intValue - 273, date: date)
    }
    
}
