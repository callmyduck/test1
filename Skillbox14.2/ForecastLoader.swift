//
//  ForecastLoader.swift
//  Skillbox14.2
//
//  Created by fomich on 18.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation
import Alamofire

class ForecastLoader: NSObject {
    
    func loadForecast(completion: @escaping ([Forecast]) -> Void){
        AF.request("https://api.openweathermap.org/data/2.5/forecast?q=Moscow&appid=5e903e97e5fed7ed297989398fb8b90b").responseJSON {
            response in
            if let objects = try? response.result.get(),
                let jsonDict = objects as? NSDictionary,
                let forecastList = jsonDict["list"] as? NSArray{
                var forecastTable: [Forecast] = []
                
                for data in forecastList{
                    
                    if let forecast = Forecast(data: data as! NSDictionary) {
                        forecastTable.append(forecast)
                        print(forecastTable.count)
                    }
                }
                DispatchQueue.main.async {
                    completion(forecastTable)
                }
            }
        }
    }
}
