//
//  Persistance1.swift
//  Skillbox14.2
//
//  Created by fomich on 17.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation
import RealmSwift



class ToDo: Object {
    
    @objc dynamic var name = ""
    let todos = List<ToDo>()
    
}
class Persistance1 {
    
    static let shared = Persistance1()
    
    private let realm = try! Realm()
    
    lazy var allToDos = realm.objects(ToDo.self)
    
    func addToDo(name: String) {
        let todo = ToDo()
        todo.name = name
        
        try! realm.write{
            realm.add(todo)
        }
    }
    func deleteToDo(index: Int){
        let toDoToDelete = allToDos[index]
        try! realm.write{
            realm.delete(toDoToDelete)
        }
    }
}
