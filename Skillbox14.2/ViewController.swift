//
//  ViewController.swift
//  Skillbox14.2
//
//  Created by fomich on 17.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var userSurnameText: UITextField!
    
    
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBAction func saveDataButton(_ sender: Any) {
        if userNameText.text == "" || userSurnameText.text == ""{
            errorLabel.isHidden = false
        }else{
            errorLabel.isHidden = true
            Persistance.shared.userName = userNameText.text
            Persistance.shared.userSurame = userSurnameText.text
            print ("\(Persistance.shared.userName)")
            print ("\(Persistance.shared.userSurame)")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        errorLabel.isHidden = true
        userNameText.text = Persistance.shared.userName
        userSurnameText.text = Persistance.shared.userSurame
        
        print(Persistance.shared.userName)
        print(Persistance.shared.userSurame)
        
        
    }
}
