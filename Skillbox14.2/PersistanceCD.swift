//
//  PersistanceCD.swift
//  Skillbox14.2
//
//  Created by fomich on 18.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation
import CoreData

class PersistanceCD {
    
    var todos = [ToDoList]()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func saveToDos(){
        do{
            try context.save()
        }catch{
            print("Error saving new todo")
        }
    }
    
    
}
