//
//  Weather.swift
//  Skillbox14.2
//
//  Created by fomich on 18.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

import UIKit

class Weather {
    
    let temp: Int
    
    init?(data: NSDictionary){
        guard let temp = data["temp"] as? NSNumber
            else { return nil }
        self.temp = temp.intValue - 273
        PersistanceWeather.shared.addWeather(temp: temp.intValue - 273)
    }
}
