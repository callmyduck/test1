//
//  PersistanceWeather.swift
//  Skillbox14.2
//
//  Created by fomich on 18.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation
import RealmSwift

class WeatherR: Object {
    
    @objc dynamic var temp = 0
    let weathers = List<WeatherR>()
    
}

class ForecastR: Object {
    
    @objc dynamic var temp = 0
    @objc dynamic var date = ""
    let temps = List<ForecastR>()
    
}

class PersistanceWeather {
    
    
    static let shared = PersistanceWeather()
    
    private let realm = try! Realm()
    
    lazy var weathers = realm.objects(WeatherR.self)
    lazy var temps = realm.objects(ForecastR.self)
    
    func addWeather(temp: Int){
        let weather = WeatherR()
        weather.temp = temp
        try! realm.write{
            realm.add(weather)
        }
    }
    
    func addForecast(temp: Int, date: String) {
        let forecast = ForecastR()
        forecast.temp = temp
        forecast.date = date
        
        try! realm.write{
            realm.add(forecast)
        }
    }
    func deleteForecast(){
        try! realm.write{
            realm.deleteAll()
        }
    }
}

