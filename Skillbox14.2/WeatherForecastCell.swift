//
//  WeatherForecastCell.swift
//  Skillbox14.2
//
//  Created by fomich on 18.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

class WeatherForecastCell: UITableViewCell {
    
    
    @IBOutlet weak var forecastNumber: UILabel!
    
    
    @IBOutlet weak var forecastDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
