//
//  WeatherNowLoader.swift
//  Skillbox14.2
//
//  Created by fomich on 18.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation
import Alamofire

class WeatherNowLoader {
    
    func loadWeatherNow(completion: @escaping (Weather) -> Void){
        
        AF.request("https://api.openweathermap.org/data/2.5/weather?q=Moscow&appid=5e903e97e5fed7ed297989398fb8b90b").responseJSON {
            response in
            if let objects = try? response.result.get(),
                let jsonDict = objects as? NSDictionary{
                for (_, data) in jsonDict where data is NSDictionary{
                    if let weatherNow = Weather(data: data as! NSDictionary) {
                        DispatchQueue.main.async {
                            completion(weatherNow)
                        }
                    }
                }
            }
        }
    }
}
