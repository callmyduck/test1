//
//  WeatherViewController.swift
//  Skillbox14.2
//
//  Created by fomich on 18.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {
    
    
    let myRefreshControl: UIRefreshControl = {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        return refreshControl
        
    }()
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var weatherNowLabel: UILabel!
    
    var forecasts: [Forecast] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.refreshControl = myRefreshControl
        
        WeatherNowLoader().loadWeatherNow { weatherNow in
            
            let weather = PersistanceWeather.shared.weathers[0]
            self.weatherNowLabel.text = "\(weather.temp)°"
        }
        
        ForecastLoader().loadForecast {
            forecast in
            self.forecasts = forecast
            self.tableView.reloadData()
        }
    }
    
    @objc private func refresh(sender: UIRefreshControl){
        
        PersistanceWeather.shared.deleteForecast()
        
        WeatherNowLoader().loadWeatherNow { weatherNow in
            
            let weather = PersistanceWeather.shared.weathers[0]
            self.weatherNowLabel.text = "\(weather.temp)°"
        }
        
        ForecastLoader().loadForecast {
            forecast in
            self.forecasts = forecast
            self.tableView.reloadData()
        }
        tableView.reloadData()
        sender.endRefreshing()
    }
    
}

extension WeatherViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return PersistanceWeather.shared.temps.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell") as! WeatherForecastCell
        let cells = PersistanceWeather.shared.temps[indexPath.row]
        cell.forecastNumber.text = "\(cells.temp)°"
        cell.forecastDate.text = cells.date
        return cell
    }
}
