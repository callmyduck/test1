//
//  ToDoViewController.swift
//  Skillbox14.2
//
//  Created by fomich on 17.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import UIKit

class ToDoViewController: UIViewController {
    
    
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var toDoText: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    @IBAction func addToDo(_ sender: Any) {
        
        if toDoText.text?.isEmpty ?? true {
            mainLabel.text = "Fill the field below"
        } else {
            Persistance1.shared.addToDo(name: "\(toDoText.text!)")
            toDoText.text = nil
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}


extension ToDoViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Persistance1.shared.allToDos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoCell") as! ToDoCell
        
        let cells = Persistance1.shared.allToDos[indexPath.row]
        
        cell.toDoLabel.text = cells.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete{
        Persistance1.shared.allToDos[indexPath.row]
            let index = indexPath.row
            Persistance1.shared.deleteToDo(index: index)
            tableView.reloadData()
        }
    }
}
