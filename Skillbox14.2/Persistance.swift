//
//  Persistance.swift
//  Skillbox14.2
//
//  Created by fomich on 17.03.2020.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation


class Persistance {
    static let shared = Persistance()
    
    private let kUserNameKey = "Persistance.kUserNameKey"
    private let kUserSurnameKey = "Persistance.kUserSurameKey"
    
    var userName: String? {
        
        set { UserDefaults.standard.set(newValue, forKey: "kUserNameKey") }
        get { return UserDefaults.standard.string(forKey: "kUserNameKey") }
        
        
    }
    var userSurame: String? {
        
        set { UserDefaults.standard.set(newValue, forKey: "kUserSurameKey") }
        get { return UserDefaults.standard.string(forKey: "kUserSurameKey") }
        
        
    }
}



